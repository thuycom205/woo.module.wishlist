<?php 
if(!defined('ABSPATH')) {
	header('HTTP/1.0 404 Not Found', true, 404);
	exit;
}
class WC_Settings_Tab {
	
	public function __construct() {
	add_filter( 'woocommerce_settings_tabs_array',array($this,'add_settings_tab'), 50 );
    add_action( 'woocommerce_settings_tabs_settings_tab',array($this,'settings_tab' ));
    add_action( 'woocommerce_update_options_settings_tab', array($this,'update_settings') );
	}
    
    function add_settings_tab($settings_tabs) {

        $settings_tabs['settings_tab'] = __( 'Settings Wishlist', 'woocommerce-settings-tab' );
        return $settings_tabs;
    }
    
    function settings_tab() {
    	woocommerce_admin_fields( self::get_settings() );
    }
    
    function update_settings() {
    	woocommerce_update_options( self::get_settings() );
    }
    
    
    
function get_settings() {
	 return array(
			array(
					'id'    => 'general-options',
					'type'  => 'title',
					'title' => __( 'General Options', 'mgwl' ),
			),
	 		
	 		array(
	 				'type' => 'single_select_page',
	 				'id' => 'mgwl_wishlist_page',
	 				'class' => 'chosen_select_nostd',
	 				'title' => __('Select wishlist page','mgwl'),
	 				'desc' => '<br>' . __('Select a page which will display wishlist settings','mgwl'),
	 		),

			array(
					'type'    => 'text',
					'id'      => 'mgwl_wishlist_text',
					'title'   => __( 'Wishlist button text', 'wccm' ),
					'desc'    => '<br>' . __( 'Enter text which will be displayed on the add to wishlist button.', 'mgwl' ),
					'default' => __( 'Wishlist', 'mgwl' ),
			),
	 		array(
	 			'type'	=> 'checkbox',
	 			'id'    => 'mgwl_send_email',
	 			'title' => __('Send email to customers','mgwl'),
	 			'desc'  => __('Enable auto send email to customers who have product is sale in their wishlist'),
	 				'default' => 'yes',
	 		),
	 		array(
	 				'type' => 'textarea',
	 				'id' => 'mgwl_email_content',
	 				'title' => __('Email content','mgwl'),
	 				'css' => ('width:400px; height:200px'),
	 				'desc' => __('Enter html email'),
	 		),
			
			 array( 'type' => 'sectionend', 'id' => 'general-options' ),

	);
	
}
}

new WC_Settings_Tab();