<?php
if(!defined('ABSPATH')) {
	header('HTTP/1.0 404 Not Found',true, 404);
	exit;
}

add_action('woocommerce_before_shop_loop','mgwl_register_add_wishlist_button_hook');
add_action('woocommerce_single_product_summary','mgwl_add_single_product_wishlist_button',35);
// add_action('template_redirect','mgwl_process_button_action');

function mgwl_register_add_wishlist_button_hook() {

		add_action('woocommerce_after_shop_loop_item','mgwl_add_button');
		add_action('woocommerce_after_shop_loop','mgwl_deregister_add_wishlist_button_hook');

}

function mgwl_add_button() {
	
	include_once WISHLIST_PATH .'model/class_wishlist.php';
	$wishlist = new Wishlist();
	$product_id = get_the_ID();

	if(!$wishlist->product_in_wishlist($product_id, get_current_user_id())) {
	$text = get_option( 'mgwl_wishlist_text', __( 'Add to wishlist', 'mgwl' ) );
	?>
		<p>
		<a href="#" class="mgwl-btn" data-product="<?php echo $product_id; ?>"><?php _e( $text, 'mgwl' ); ?></a>
		<p>
	<?php

}
else {
	?>
		<div class="mgwl-text">
		<p>Already on wishlist</p>
		</div>
	<?php
}
}
function mgwl_add_single_product_wishlist_button() {

		echo '<p>';
		mgwl_add_button();
		echo '</p>';

}
function mgwl_deregister_add_wishlist_button_hook() {
	remove_action( 'woocommerce_after_shop_loop_item', 'mgwl_add_button' );
}

