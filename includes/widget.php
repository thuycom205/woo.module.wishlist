<?php
if(!defined('ABSPATH')) {
	header('HTTP/1.0 404 Not Found',true,404);
}

add_action('widgets_init','mgwl_widgets_init');

function mgwl_widgets_init() {
	register_widget('MGWL_Widget_Recent_Wishlist');
}

class MGWL_Widget_Recent_Wishlist extends WP_Widget {
	
	public function __construct() {
		parent::__construct('mgwl_recent_wishlist',esc_html__( 'WooCommerce Recent Wishlist', 'mgwl' ), array(
			'description' => esc_html__( 'Shows a list of user most recent wishlist on your site.', 'mgwl' ),
		) );
	}
	
	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$width = isset( $instance['width'] ) ? esc_attr( $instance['width'] ) : '';
		$height = isset( $instance['height'] ) ? esc_attr( $instance['height'] ) : '';
	
		?><p><label for="<?php echo $this->get_field_id( 'title' ) ?>"><?php esc_html_e( 'Title:', 'mgwl' ) ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ) ?>" name="<?php echo $this->get_field_name( 'title' ) ?>" type="text" value="<?php echo $title ?>"></p>
	
			<p><label for="<?php echo $this->get_field_id( 'width' ) ?>"><?php esc_html_e( 'Thumbnails Width:', 'mgwl' ) ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'width' ) ?>" name="<?php echo $this->get_field_name( 'width' ) ?>" type="text" value="<?php echo $width ?>"></p>
	
			<p><label for="<?php echo $this->get_field_id( 'height' ) ?>"><?php esc_html_e( 'Thumbnails Height:', 'mgwl' ) ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'height' ) ?>" name="<?php echo $this->get_field_name( 'height' ) ?>" type="text" value="<?php echo $height ?>"></p><?php
}
public function widget( $args, $instance ) {

	global $wpdb;
	$thumbs = array();
	$filter_options = array(
			'options' => array(
					'min_range' => 16,
					'default'   => 48,
			),
	);

	$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Wishlist', 'mgwl' ) : $instance['title'], $instance, $this->id_base );
	$width = !empty( $instance['width'] ) ? filter_var( $instance['width'], FILTER_VALIDATE_INT, $filter_options ) : 48;
	$height = !empty( $instance['height'] ) ? filter_var( $instance['height'], FILTER_VALIDATE_INT, $filter_options ) : 48;

	echo $args['before_widget'];
	echo $args['before_title'], $title, $args['after_title'];


	$user_id = get_current_user_id();
	$items =  $wpdb->get_results( 'SELECT product_id FROM ' . Magenest_Wishlist::$table_name . ' WHERE user_id = ' . $user_id);
	
		echo '<p>';
		echo '<a href="', get_permalink( get_option('woocommerce_myaccount_page_id') ), '">';
		foreach( $items as $item ) {
			if ( !isset( $thumbs[$item->product_id] ) ) {
				$thumbs[$item->product_id] = get_the_post_thumbnail( $item->product_id, array( $width, $height ) );
			}

			echo $thumbs[$item->product_id];
		}
		echo '</a>';
		echo '</p>';
	
	echo $args['after_widget'];

}
}