jQuery(document).ready(function() {

	jQuery('.mgwl-btn').click(function(e) {
		e.preventDefault();
		var this_button = jQuery(this);
		var product_id = jQuery(this).data('product');
		this_button.off('click');
		this_button.addClass('mgwl-btn-disabled');
	    post_data = 'action=addtowishlist&p=' + product_id;
	    jQuery.ajax({
			type: 'post',
			url: mgwl_ajaxurl,
			data: post_data,
			dataType: 'json',
			error: function(XMLHttpRequest, textStatus, errorThrown){
				this_button.addClass('mgwl-btn-ko');
				this_button.text(mgwl_msg_ko);				
			},
			success: function(data, textStatus){
				if(data.response && data.response == 'OK') {
					this_button.addClass('mgwl-btn-ok');
					alert('Complete add to wishlist!');
					this_button.text(mgwl_msg_ok);
				} else {
					this_button.addClass('mgwl-btn-ko');
					alert('Error');
					this_button.text(mgwl_msg_ko);
				}			
			}
		});
	});
	
});
