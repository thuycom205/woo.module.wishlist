jQuery(document).ready(function() {

	jQuery('a.delete').click(function(e) {
		e.preventDefault();
		
		var this_button = jQuery(this);
		var product_id = jQuery(this).data('product');

	    post_data = 'action=removefromwishlist&delete=' + product_id;
	    jQuery.ajax({
			type: 'post',
			url: mgwl_ajaxurl,
			data: post_data,
			dataType: 'json',
			success: function(data, textStatus){
				if(data.response && data.response == 'OK') {

					alert('Complete delete!');
					 location.reload(); 

				} else {

					alert('Error');

				}			
			}
		});
	});
	
});
