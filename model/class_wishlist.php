<?php

if( !class_exists( 'Wishlist' ) ) {
	
	class Wishlist {

		function Wishlist() {}

		public function product_in_wishlist( $product_id, $user_id ) {

			global $wpdb;

			$item =  $wpdb->get_var( 'SELECT product_id FROM ' . Magenest_Wishlist::$table_name . ' WHERE user_id = ' . $user_id . ' AND product_id = ' . $product_id );

			if( $item == NULL ) {
				return false;
			} else {
				return true;
			}

		}	

		function show_button( $product_id ) {
			$text = get_option( 'mgwl_wishlist_text', __( 'Add to my wishlist', 'mgwl' ) );
			echo '<p>';
			?>

			<div class="summary">
				<a href="#" class="mgwl-btn" data-product="<?php echo $product_id; ?>"><?php _e( $text, 'mgwl' ); ?></a>
			</div>
			
			<?php
			echo '</p>';
		}	

		function add_product_to_wishlist( $product_id, $user_id ) {

			global $wpdb;

			$result = $wpdb->insert( 
				Magenest_Wishlist::$table_name, 
				array( 
					'product_id' => $product_id, 
					'user_id' => $user_id
				), 
				array( 
					'%d', 
					'%d' 
				) 
			);

			if( !$result ) {
				return false;
			} else {
				return true;
			}

		}	
		
		function render_list( $user_id ) {
		
			global $wpdb;
		
			$items =  $wpdb->get_results( 'SELECT product_id FROM ' . Magenest_Wishlist::$table_name . ' WHERE user_id = ' . $user_id);
			
			if( count( $items ) > 0 ) {
				echo '<div class="mgwl-wishlist"><h2>' . __( 'My wishlist', 'mgwl' );
				
				echo '<table class="mgwl-table"><thead><tr><th colspan="3">' . __( 'Product', 'mgwl' ) . '</th>';
				echo '<th>' . __( 'Price', 'mgwl' ) . '</th></tr></thead><tbody>';
				foreach( $items as $item ) {
					$the_post = get_post( $item->product_id );
					$the_product = new WC_Product( $item->product_id );
					$product_name = $the_post->post_title;
					$product_thumbnail = get_the_post_thumbnail( $item->product_id, 'thumbnail' );
					$product_price = $the_product->get_price_html();
					$product_permalink = get_the_permalink( $item->product_id );
		
					echo '<tr><td><a href="' . $product_permalink . '">' . $product_thumbnail . '</a></td><td><a href="' . $product_permalink . '">' . $product_name . '</a></td><td>' . $product_price . '</td>
				<td>
				<a href="#" data-product="'.$item->product_id.'" class="delete">Delete</a>
				
				</td></tr>';
				}
				echo '</tbody></table>';
				echo '</div>';
				
			} else {
				echo '<h3>' . __( 'Your wishlist is empty', 'mgwl' ) . '</h3>';
			}
		
		}

		function remove_product_from_wishlist( $product_id ) {
			
			global $wpdb;

			$result = $wpdb->delete( 
				Magenest_Wishlist::$table_name, 
				array( 
					'product_id' => $product_id
				), 
				array( 
					'%d'
				) 
			);
		
			if( !$result ) {
				return false;
			} else {
				return true;
			}
		
		}
		
	}
}
new Wishlist();