<?php
/**
 * Plugin Name: WooCommerce Wishlist
* Plugin URI: http://store.magenest.com/woocommerce-plugins/woocommerce-wishlist.html
* Description: Add wishlist function to website
* Author: Magenest
* Author URI: http://magenest.com
* Version: 1.0
* Text Domain: woocommerce-wishlist
* Domain Path: /languages/
*/

if (!defined('ABSPATH')) exit;  //Exit if accessed directly

if(!defined('WISHLIST_TEXT_DOMAIN'))
	define('WISHLIST_TEXT_DOMAIN','wishlist');

//Plugin Folder Path	
if(!defined('WISHLIST_PATH'))
	define('WISHLIST_PATH', plugin_dir_path(__FILE__));
	
//Plugin Folder URL
if(!defined('WISHLIST_URL'))
	define ('WISHLIST_URL', plugins_url ( 'woocommerce-wishlist', 'woocommerce-wishlist.php' ) );

//Plugin Root File
if(!defined('WISHLIST_FILE'))
	define('WISHLIST_FILE', plugin_basename(__FILE__));
	
class Magenest_Wishlist {
	private static $wishlist_instance;
	private static $wishlist;
	public  static $table_name;
	
	public function __construct() {
		global $wpdb;
		self::$table_name = $wpdb->prefix . 'magenest_wishlists';
		include_once WISHLIST_PATH .'model/class_wishlist.php';
		
		self::$wishlist = new Wishlist();
		
		load_plugin_textdomain( 'mgwl', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		require_once 'includes/widget.php';
		require_once 'includes/buttons.php';
		if(is_admin()) {
			require_once 'admin/settings.php';
		}
		
		register_activation_hook (WISHLIST_FILE, array ($this,'install' ) );
		add_action('wp_enqueue_scripts',array($this,'load_js_css'));
		add_action('wp_ajax_removefromwishlist',array($this,'remove_wishlist'));
		add_action('wp_ajax_addtowishlist',array($this,'add_to_wishlist'));
// 		add_action('woocommerce_after_add_to_cart_button',array($this,'add_button_wishlist'),5);
// 		add_action('woocommerce_after_my_account',array($this,'render_wishlist'));
		add_action('wp_head',array($this,'add_js_vars'));
		add_filter('the_content',array($this,'mgwl_render_wishlist_page'));
		
		
	}
	public function install() {
		global $wpdb;
		$wpdb->query('CREATE TABLE `' . self::$table_name . '` (
  					`user_id` bigint(20) NOT NULL,
  					`product_id` bigint(20) NOT NULL,
  					PRIMARY KEY (`user_id`,`product_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
	}
	
	public function add_button_wishlist() {
		if(is_user_logged_in() ) {
			global $product;
			if(!self::$wishlist->product_in_wishlist($product->post->ID, get_current_user_id())) {
				self::$wishlist->show_button($product->post->ID);
				
			}
		}
	}
	public function add_to_wishlist() {
		$response = false;
		if(is_user_logged_in() ) {
			$product_id = floatval($_POST['p']);
			if($product_id > 0) {
				$response = self::$wishlist->add_product_to_wishlist($product_id, get_current_user_id());
			}
		}
		if($response) {
			die(json_encode(array(response => 'OK')));
		}
		else {
			die(json_encode(array(response => 'KO')));
		}	
	}
	function render_wishlist() {
	
		if( is_user_logged_in() ) {
			echo '<div class="mgwl-wishlist"><h2>' . __( 'My wishlist', 'mgwl' );
			self::$wishlist->render_list( get_current_user_id() );
			echo '</div>';
		}		
	}

	function load_js_css() {
	
		wp_register_script( 'mgwl-main', plugins_url( 'assets/main.js', __FILE__ ), array( 'jquery' ), true );
		wp_enqueue_script( 'mgwl-main' );
		wp_register_script( 'mgwl-delete', plugins_url( 'assets/delete.js', __FILE__ ), array( 'jquery' ), true );
		wp_enqueue_script( 'mgwl-delete' );
		wp_register_style( 'mgwl-css', plugins_url( 'assets/styles.css', __FILE__ ), array(), false );
		wp_enqueue_style( 'mgwl-css' );
	
	}
	function add_js_vars() {
	
		?>
			<script type="text/javascript">
				var mgwl_ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
				var mgwl_msg_ok = '<?php _e( 'Added to wishlist!', 'mgwl' ); ?>';
				var mgwl_msg_ko = '<?php _e( 'ERROR!', 'mgwl' ); ?>';
			</script>
		<?php
	}
	
	public function remove_wishlist() {
		global $wpdb;
		$response = false;
		if(is_user_logged_in() ) {
		if(isset($_POST['delete'])) {
		$product_id = floatval($_POST['delete']);
		$response = self::$wishlist->remove_product_from_wishlist($product_id);
		
    }
	
  }
  if($response) {
  	die(json_encode(array(response => 'OK')));
  }
  else {
  	die(json_encode(array(response => 'KO')));
  }
	}
	
	public function mgwl_render_wishlist_page($content) {
		if( is_user_logged_in() ) {
		if(!is_page() || get_option('mgwl_wishlist_page') != get_the_ID()) {
			return $content;
		}
		return $content . self::$wishlist->render_list( get_current_user_id() );
	}
	}
	
	// kiểm tra xem liệu đối tượng class đã được tạo chưa (thông qua thuộc tính
	public static function getInstance() {
		if (! self::$wishlist_instance) {
			// nếu chưa thì tạo mới một đối tượng
			self::$wishlist_instance = new Magenest_Wishlist();
		}
		// Trả về chính đối tượng đã được tạo trước đó
		return self::$wishlist_instance;
	}
}
new Magenest_Wishlist();
?>